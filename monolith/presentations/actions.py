import pika, json


def queue_email(presentation, toggle):
    dict = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }

    message = json.dumps(dict)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    if toggle:
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_publish(
            exchange="", routing_key="presentation_approvals", body=message
        )
    else:
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_publish(
            exchange="", routing_key="presentation_rejections", body=message
        )

    connection.close()
