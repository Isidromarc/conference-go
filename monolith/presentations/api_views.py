from django.http import JsonResponse
from common.myEncoders import PresentationEncoder, PresentationListEnconder
from .models import Presentation
from events.models import Conference
from django.views.decorators.http import require_http_methods
from .actions import queue_email
import json


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEnconder
        )
    else:
        print(request.body)
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"}, status=400
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationEncoder, safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"meesage": "Invalid Conference ID"})

        if "status" in content:
            content.pop("status")

        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation, encoder=PresentationEncoder, safe=False
        )


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    queue_email(presentation, True)
    return JsonResponse(presentation, encoder=PresentationEncoder, safe=False)


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    queue_email(presentation, False)
    return JsonResponse(presentation, encoder=PresentationEncoder, safe=False)
