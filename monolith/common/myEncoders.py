from json import JSONEncoder
from datetime import datetime
from events.models import Conference, Location, State
from presentations.models import Presentation, Status
from django.db.models import QuerySet
from accounts.models import User
from django.utils import timezone


class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateTimeEncoder, QuerySetEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()

            for property in self.properties:
                value = getattr(o, property)

                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
    ]

    encoders = {"location": LocationListEncoder()}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class StateEncoder(ModelEncoder):
    model = State
    properties = [
        "abbreviation",
    ]


class LocationEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationListEnconder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class AccountModelEncoder(ModelEncoder):
    model = User
    properties = ["email", "first_name", "last_name"]


class AccountInfoModelEncoder(ModelEncoder):
    model = User
    properties = ["email", "first_name", "last_name", "is_active"]

    def get_extra_data(self, o):
        return {"updated": timezone.now()}
