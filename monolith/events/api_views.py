from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Conference, Location, State
from common.myEncoders import (
    ConferenceEncoder,
    ConferenceListEncoder,
    LocationListEncoder,
    LocationEncoder,
)
import json
from events.acls import get_pexel_picture_url, get_weather


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()

        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location, does not exist"}, status=400
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(conference, encoder=ConferenceEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            {
                "Weather": get_weather(conference.location),
                "Conference": conference,
            },
            encoder=ConferenceEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"}, status=400
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(conference, encoder=ConferenceEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)
        content["picture"] = get_pexel_picture_url(content["city"])
        try:
            location = Location.create(**content)
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid State"}, status=400)
        return JsonResponse(
            location,
            encoder=LocationEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        try:
            location = Location.objects.get(id=id)
            return JsonResponse(location, encoder=LocationEncoder, safe=False)
        except Location.DoesNotExist:
            return JsonResponse({"message": "ID does not exist"}, status=400)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid State Abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationEncoder,
            safe=False,
        )
