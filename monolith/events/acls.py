import requests
import json
from api_keys import pexelkey, owmkey


def get_pexel_picture_url(location):
    query = {"query": location}
    headers = {"Authorization": pexelkey}
    response = requests.get(
        "https://api.pexels.com/v1/search", params=query, headers=headers
    )
    if response.status_code != 200:
        return ""
    results = json.loads(response.text)
    photo_url = results["photos"][0]["src"]["original"]
    return photo_url


def get_coords(city, stateabbr, country="US"):
    query = {"q": f"{city},{stateabbr},{country}", "appid": owmkey}
    response = requests.get(
        "http://api.openweathermap.org/geo/1.0/direct",
        params=query,
    )
    if response.status_code != 200:
        return {}

    results = json.loads(response.text)
    coords = {}
    coords["lat"] = results[0]["lat"]
    coords["lon"] = results[0]["lon"]

    return coords


def get_weather(location):
    city = location.city
    stateabbr = location.state.abbreviation

    coords = get_coords(city, stateabbr)
    # units: default=Kelvin, imperial = Farh, metric = Cels
    query = {
        "lat": coords["lat"],
        "lon": coords["lon"],
        "appid": owmkey,
        "units": "imperial",
    }

    response = requests.get(
        "https://api.openweathermap.org/data/2.5/weather", params=query
    )

    if response.status_code != 200:
        return ""

    results = json.loads(response.text)
    weather = {
        "temp": results["main"]["temp"],
        "description": results["weather"][0]["description"],
    }

    return weather
