from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendeeservice.settings")
django.setup()

from attendees.models import AccountVO


def process_message(ch, method, properties, body):
    content = json.loads(body.decode())
    properties = ["first_name", "last_name", "email", "is_active", "updated"]
    if content.get("email") is not None:
        default = {}
        for field in properties:
            if content.get(field) is not None:
                default[field] = content[field]

        if default.get("updated") is not None:
            default["updated"] = datetime.fromisoformat(default["updated"])

        if default.get("is_active"):
            AccountVO.objects.update_or_create(
                email=default["email"], defaults=default
            )
            print("account created/editted")
        else:
            try:
                AccountVO.objects.get(email=default["email"]).delete()
                print("deleted")
            except AccountVO.DoesNotExist:
                print("no account found")
                pass


def consume_messages():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="account_info", exchange_type="fanout")
    result = channel.queue_declare(queue="", exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(queue=queue_name, exchange="account_info")

    channel.basic_consume(
        queue=queue_name, on_message_callback=process_message, auto_ack=True
    )

    channel.start_consuming()


while True:
    try:
        consume_messages()
    except AMQPConnectionError:
        print("AccountVO: Connecting...")
        time.sleep(2.0)
