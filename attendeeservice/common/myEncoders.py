from json import JSONEncoder
from datetime import datetime
from attendees.models import Attendee, ConferenceVO, AccountVO
from django.db.models import QuerySet


class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateTimeEncoder, QuerySetEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()

            for property in self.properties:
                value = getattr(o, property)

                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class ConferenceVOEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]

    encoders = {
        "conference": ConferenceVOEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        if count:
            return {"has_account": True}
        else:
            return {"has_account": False}


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]
