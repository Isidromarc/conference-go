import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os, sys, time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval_mail(ch, method, properties, body):
    message = json.loads(body.decode("utf-8"))
    email_title = "Your presentation has been accepted"
    to = [message["presenter_email"]]
    name = message["presenter_name"]
    title = message["title"]
    sender = "admin@conference.go"
    content = f"{name}, we're happy to tell you that your presentation {title} has been accepted"

    send_mail(email_title, content, sender, to, fail_silently=False)


def process_rejection_mail(ch, method, properties, body):
    message = json.loads(body.decode("utf-8"))
    email_title = "Your presentation has been rejected"
    to = [message["presenter_email"]]
    name = message["presenter_name"]
    title = message["title"]
    sender = "admin@conference.go"
    content = f"{name}, we're sorry to tell you that your presentation {title} has been rejected"

    send_mail(email_title, content, sender, to, fail_silently=False)


def process_messages():

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.queue_declare(queue="presentation_rejections")

    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval_mail,
    )
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection_mail,
    )

    print("Starting process...")
    channel.start_consuming()


while True:
    try:
        process_messages()
    except AMQPConnectionError:
        print("Waiting For Connection...")
        time.sleep(2.0)
